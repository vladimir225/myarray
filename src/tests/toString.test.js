import MyArray from '../index';

describe('tests for toString()', () => {
  test('should return a string with commas if array is empty, but length > 0', () => {
    const arr = new MyArray(5);
    expect(arr.toString()).toBe(',,,,');
  });

  test('should ignore undefined and null values', () => {
    const arr = new MyArray(NaN, 1, undefined, 3, null, 25);
    expect(arr.toString()).toBe('NaN,1,,3,,25');
  });

  test('should work with NaN, function, object', () => {
    const arr = new MyArray(NaN, () => true, {});
    expect(arr[0].toString()).toBe('NaN');
    expect(arr[1].toString()).toBe('() => true');
    expect(arr[2].toString()).toBe('[object Object]');
  });

  test('should return a string', () => {
    const arr = new MyArray(5);
    expect(typeof arr.toString()).toBe('string');
  });

  test('should return an empty string if array length equal 0', () => {
    const arr = new MyArray();
    expect(arr.toString()).toBe('');
  });

  test('method should not have an argument', () => {
    const arr = new MyArray(1, 2, 3);
    expect(arr.toString).toHaveLength(0);
  });
});
